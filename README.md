# Translations with React
## Without hook
```jsx
import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';

class Content extends Component {
	render() {
		const { i18n, t } = this.props;

		return (
			<div>
				<p>{i18n.t('The cake is a lie')}</p>
				<p>{t('Swords, not words!')}</p>
			</div>
		);
	}
}

export default withTranslation()(Content);
```

## With hook
```jsx
import React from 'react';
import { useTranslation } from 'react-i18next';

const Hook = () => {
	const { t, i18n } = useTranslation();

	return(
		<div>
			<h2>{t('With hook')}</h2>
			<p>{i18n.t('Content Hook component')}</p>
		</div>
	);
}

export default Hook;
```

## Plural
```jsx
<p>{i18n.t('You have {{ count }} unread message', { count })}</p>
```

### In the translations files
```json
{
	"You have {{ count }} unread message": "Vous avez {{ count }} messages non lu",
	"You have {{ count }} unread message_plural": "Vous avez {{ count }} messages non lus"
}
```


## Variables
```js
<p>{i18n.t('Welcome {{ name }}!', { name })}</p>
```

### In the translations files
```json
{
	"Welcome {{ name }}!": "Bienvenue {{ name }}!"
}
```

## Placeholder
```html
<input type="text" placeholder={i18n.t('Placeholder')} />
```

## Custom translation tag
In the i18next-scanner.config.js file
```
func: {
	list: ['i18next.t', 'i18n.t', 't', '__'],
	extensions: ['.js', '.jsx']
}
```
### Example
```js
<p>{__('The cake is a lie')}</p>
```

## Trans component
Requires a key
```js
<Trans i18nKey="lorem.ipsum">Lorem ipsum</Trans>
```

## Extract translations
```bash
i18next-scanner --config i18next-scanner.config.js 'src/**/*.{js,jsx}'
```

## Built with
* [react-i18next](https://github.com/i18next/react-i18next)
