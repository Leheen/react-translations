module.exports = {
	options: {
		debug: true,
		removeUnusedKeys: false,
		func: {
			list: ['i18next.t', 'i18n.t', 't', '__'],
			extensions: ['.js', '.jsx']
		},
		trans: {
			component: 'Trans',
			extensions: ['.js', '.jsx'],
			fallbackKey: true
		},
		lngs: ['en', 'fr'],
		ns: ['translation'],
		defaultLng: 'en',
		defaultNs: 'translation',
		defaultValue: '',
		resource: {
			loadPath: 'public/locales/{{lng}}/{{ns}}.json',
			savePath: 'public/locales/{{lng}}/{{ns}}.json',
			jsonIndent: 2,
			lineEnding: '\n'
		},
		nsSeparator: false, // namespace separator
		keySeparator: false, // key separator
		interpolation: {
			prefix: '{{',
			suffix: '}}'
		}
	}
};