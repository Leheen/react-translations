import React, { Component, Suspense } from 'react';

import Navbar from './components/Navbar';
import Content from './components/Content';
import Hook from './components/Hook';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Suspense fallback={<div />}>
          <Navbar />
          <Content />
          <Hook />
        </Suspense>
      </div>
    );
  }
}

export default App;
