import React, { Component } from 'react';
import { withTranslation } from 'react-i18next';

class Navbar extends Component {
	changeLanguage = lng => {
		const { i18n } = this.props;
		i18n.changeLanguage(lng);		
	}

	render() {
		const { i18n } = this.props;

		return (
			<nav>
				<button onClick={() => this.changeLanguage('en')}>{i18n.t('English')}</button>
				<button onClick={() => this.changeLanguage('fr')}>{i18n.t('French')}</button>
			</nav>
		);
	}
}

export default withTranslation()(Navbar);
