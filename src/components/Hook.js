import React from 'react';
import { useTranslation } from 'react-i18next';

const Hook = () => {
	const { t, i18n } = useTranslation();

	return(
		<div>
			<h2>{t('With hook')}</h2>
			<p>{i18n.t('Content Hook')}</p>
		</div>
	);
}

export default Hook;
