import React, { Component } from 'react';
import { Trans, withTranslation } from 'react-i18next';

class Content extends Component {
	render() {
		const { i18n, t } = this.props;
		const count = 2;
		const name = "2B";
		const __ = t;

		return (
			<div>
				<h1>{i18n.t('Title')}</h1>
				<p>{i18n.t('The cake is a lie.')}</p>
				<p>{t('Swords, not words!')}</p>

				{/* Placeholder */}
				<input type="text" placeholder={i18n.t('Placeholder')} />

				{/* Plural and variables */}
				<p>{i18n.t('You have {{ count }} unread message', { count })}</p>
				<p>{i18n.t('Welcome {{ name }}!', { name })}</p>

				{/* Custom translation tag */}
				<p>{__('Heroes never die.')}</p>

				{/* Trans component */}
				<Trans i18nKey="lorem.ipsum">Lorem ipsum</Trans>
			</div>
		);
	}
}

export default withTranslation()(Content);
